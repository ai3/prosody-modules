ai3: Prosody Modules
====================

This is a fork of the [official community modules mercurial repository][1].
There are currently two modules patched:
 - [`mod_net_proxy`](./mod_net_proxy/mod_net_proxy.lua)
 - [`mod_auth_custom_http`](./mod_auth_custom_http/mod_auth_custom_http.lua)

This repo is used to build our own [conteinerized prosody][2]


[1]: https://hg.prosody.im/prosody-modules
[2]: https://git.autistici.org/ai3/docker/prosody
