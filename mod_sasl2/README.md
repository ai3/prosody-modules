---
labels:
- Stage-Alpha
summary: "XEP-0388: Extensible SASL Profile"
---

Experimental implementation of [XEP-0388: Extensible SASL Profile]
